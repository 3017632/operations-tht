const prompt = require('prompt-sync')();
const fs = require('fs');
const axios = require('axios');

const devpostApiUrl = "https://devpost.com/api/";

async function fetchDataFromURL(url) {
    try {
        const response = await axios.get(url);
        return response.data;
    } catch (error) {
        console.error('Error fetching data:', error);
        throw error;
    }
}

function writeDataToFile(data, file) {
    try {
        const jsonData = JSON.stringify(data, null, 2);
        fs.writeFileSync(file, jsonData, 'utf8');
        console.log(`Data has been written to ${file}`);
    } catch (error) {
        console.error('Error writing file:', error);
        throw error;
    }
}


const organizationsUrl = devpostApiUrl + "organizations";
fetchDataFromURL(organizationsUrl)
    .then(data => {
        writeDataToFile(data, 'orgs.json');
        const themesUrl = devpostApiUrl + "themes";
        return fetchDataFromURL(themesUrl);
    })
    .then(data => writeDataToFile(data, 'themes.json'))
    .then(() => {
        const themes = require('./themes.json');
        const orgs = require('./orgs.json');
        let userResp = prompt('Do you want organizations or hackathons (o/h): ');

        let finUrl = devpostApiUrl;

        if (userResp === "o") {
            finUrl = finUrl + "organizations?term=";
        } else if (userResp === "h") {
            finUrl = finUrl + "hackathons?";
            console.log("FILTERS");
            userResp = parseInt(prompt("Location(Online ; In Person): (answer with 1 or 2 or 0 for none)"));
            if (userResp === 1) {
                finUrl = finUrl.concat("challenge_type[]=online&");
            } else if (userResp === 2) {
                finUrl = finUrl.concat("challenge_type[]=in-person&")
            }
            userResp = parseInt(prompt("Status(Upcoming ; Open ; Ended): (answer with 1 or 2 or 3 or 0 for none)"));
            if (userResp === 1) {
                finUrl = finUrl.concat("status[]=upcoming&");
            } else if (userResp === 2) {
                finUrl = finUrl.concat("status[]=open&");
            } else if (userResp === 3) {
                finUrl = finUrl.concat("status[]=ended&");
            }
            userResp = parseInt(prompt("Length(1-6 days ; 1-4 weeks ; 1+ month): (answer with 1 or 2 or 3 or 0 for none)"));
            if (userResp === 1) {
                finUrl = finUrl.concat("length[]=days&");
            } else if (userResp === 2) {
                finUrl = finUrl.concat("length[]=weeks&");
            } else if (userResp === 3) {
                finUrl = finUrl.concat("length[]=months&");
            }

            console.log("Interest tags. Leave blank if you want none. Select as many as you would like seperated by a ,");
            let tags = Array.from(Array(themes.length));
            themes.forEach((theme, i) => {
                console.log((i + 1) + ': ' + theme.name);
                tags[i] = theme;
            });

            let rawResp = prompt("Numbers seperated by , : ").split(',');
            if (!isNaN(rawResp[0])) {
                let respArr = Array.from(Array(rawResp.length));
                rawResp.forEach((e, i) => {
                    respArr[i] = parseInt(e.trim());
                });
                console.log(respArr);
                respArr.forEach((e) => {
                    finUrl = finUrl.concat(`themes[]=${themes[e - 1].name}&`);
                });
            }
            console.log("Organizations. Select as many as you would like seperated by a ,");

            orgs.forEach((org, i) => {
                console.log((i + 1) + ': ' + org.name);
                tags[i] = org;
            });
            rawResp = prompt("Number: ");
            if (rawResp.length >= 1) {
                finUrl = finUrl.concat(`organization=${orgs[parseInt(rawResp) - 1].name}&`);
            }

            userResp = parseInt(prompt("Open To(Public ; Invite Only): (answer with 1 or 2 or 0 for none)"));
            if (userResp === 1) {
                finUrl = finUrl.concat("open_to[]=public&");
            } else if (userResp === 2) {
                finUrl = finUrl.concat("open_to[]=invite_only&");
            }
        }

        return fetchDataFromURL(finUrl);
    })
    .then(data => {
        writeDataToFile(data, 'result.json');
        const result = require('./result.json');
        if (result.hackathons.length > 0) {
            console.log("I found some hackathons for you: ");
            result.hackathons.forEach((e, i) => {
                console.log((i + 1) + ": " + e.title);
            });
        } else {
            console.log("Sorry, there are no hackathons for u :(")
        }
    })
    .catch(error => console.error('Error:', error));
